import { Component, OnInit, NgModule } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { ModalController, PopoverController } from '@ionic/angular';
import { User } from 'src/app/models/users';
import { CommonModule } from '@angular/common';
import { ProfilePage } from '../profile/profile.page';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
@NgModule({
  imports: [CommonModule]
})
export class DashboardPage implements OnInit {
  user: User;
  constructor( private authService: AuthService,
               private modalController: ModalController,private popoverController:PopoverController) {
               }
  ngOnInit() {
  }
  ionViewWillEnter() {
    this.authService.user().subscribe(
      user => {
        console.log(user);
        this.user = user;
      }
    );
  }
  dismissLogout() {
    this.modalController.dismiss();
  }
  logout() {
    this.authService.logout();
  }
 async openPopup(){
   
    const popover=await this.popoverController.create({
      component:ProfilePage,
      // event
    });
    return await popover.present();
  }
}
