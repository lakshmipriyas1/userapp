import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { NgForm } from '@angular/forms';
import { AlertService } from '../services/alert.service';
import { AuthService } from '../services/auth.service';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  todo = {
  username: '',
  password: ''
};
  constructor(private storage: Storage,
              private authService: AuthService,
              private alertService: AlertService,
              private navCtrl: NavController,
              private router: Router,
    ) {
    }

  ngOnInit() {
  }
 async login(form: NgForm) {
    this.authService.login(form.value.username, form.value.password).subscribe(
      data => {
      },
      error => {
        console.log(error);
      },
      () => {
        this.router.navigateByUrl('/dashboard');
      }
    );
  }
}
