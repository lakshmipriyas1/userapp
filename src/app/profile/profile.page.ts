import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { User } from 'src/app/models/users';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  user:User;
  constructor(private popoverController:PopoverController,private authservice:AuthService) { }

  ngOnInit() {
  }
  ionViewWillEnter() {
    this.authservice.user().subscribe(
      user => {
        console.log(user);
        this.user = user;
      }
    );
  }
closePopup(){
  this.popoverController.dismiss();
}

}
