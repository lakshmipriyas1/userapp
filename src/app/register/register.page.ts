import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
 todo = {
    fName: '',
    lName: '',
    username: '',
    password: ''
  };
  constructor(
               private authService: AuthService,
               private router: Router
    ) { }

  ngOnInit() {
  }
  register(form: NgForm) {
    this.authService.register(form.value.fName, form.value.lName, form.value.username, form.value.password).subscribe(
        error => {
          console.log(error);
        },
        () => {
        }
    );
      }
}
